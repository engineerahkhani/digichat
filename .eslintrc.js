module.exports = {
	"parser": "babel-eslint",
	"plugins": [
	  "react",
	  "react-native",
	  "flowtype",
	  "jsx-a11y",
	  "import",
	  "prettier",
	  "sort-keys-fix",
	  "react-hooks"
	],
	"extends": [
	  "plugin:react-native/all",
	  "airbnb",
	  "prettier",
	  "prettier/flowtype",
	  "prettier/react",
	  "prettier/standard"
	],
	"settings": {
	  "import/resolver": {
		"babel-module": {},
		"node": {
		  "paths": ["src"],
		  "extensions": [
			".js",
			".android.js",
			".ios.js"
		  ]
		}
	  }
	},
	"globals": {
	  "GLOBAL": true
	},
	"rules": {
		"react/prop-types": 0,
		"react-native/no-color-literals":0,
	  "global-require": "off",
	  "no-use-before-define": ["error", { "variables": false }],
	  "no-console": "off",
	  "no-underscore-dangle": "off",
	  "function-paren-newline": "off",
	  "quotes": ["error", "single"],
	  "react/jsx-filename-extension": [
		"error",
		{
		  "extensions": [".js"]
		}
	  ],
	  "import/no-cycle": "off",
	  "import/no-extraneous-dependencies": [
		"error",
		{
		  "devDependencies": true
		}
	  ],
	  "jsx-a11y/anchor-is-valid": [
		"error",
		{
		  "specialLink": ["to"]
		}
	  ],
	  "prettier/prettier": [
		"error",
		{
		  "singleQuote": true,
		  "trailingComma": "es5"
		}
	  ],
	  "react-hooks/rules-of-hooks": "error",
	  "react-hooks/exhaustive-deps": "warn"
	},
	"env": {
	  "jest": true
	},
  "globals": {
    GLOBAL: true,
  }
}