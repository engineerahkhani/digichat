import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ChatScreen from '../Containers/ChatScreen';
import ProfileScreen from '../Containers/ProfileScreen';
import HeaderAvatar from '../Components/HeaderAvatar';
import {colors} from '../Theme';

const Stack = createStackNavigator();

const RootNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: colors.lightBlack,
            height: 64,
            shadowOpacity: 0,
            shadowOffset: {
              height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
          },
          headerTintColor: colors.white,
        }}>
        <Stack.Screen
          name="ChatScreen"
          component={ChatScreen}
          options={{
            title: null,
            headerLeft: () => <HeaderAvatar />,
          }}
        />
        <Stack.Screen
          options={{
            title: 'Profile',
          }}
          name="ProfileScreen"
          component={ProfileScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default RootNavigation;
