import image from '../Assets/avatar1.jpg';
import avatar from '../Assets/avatar.png';
import {formatDate, addMinutes, compose, getNewKey} from '../Utils';

const addMinutesAndFormat = compose(formatDate, addMinutes);

export const authUser = {
  id: 1001,
  name: 'Digi Admin',
  image,
};
export const consumer = {
  id: 2001,
  name: 'Jo Marcelo',
  image: avatar,
};
export const initMessage = [
  {
    id: getNewKey(),
    user: authUser,
    message: 'welcome to Digi Chat, how can I help you?',
    date: formatDate(new Date()),
  },
  {
    id: getNewKey(),
    user: consumer,
    message: 'what is my order status right now?',
    date: addMinutesAndFormat(new Date(), 2),
  },
  {
    id: getNewKey(),
    user: authUser,
    message: 'ok, your order id please?',
    date: addMinutesAndFormat(new Date(), 3),
  },
  {
    id: getNewKey(),
    user: consumer,
    message: 'my order id is A230054',
    date: addMinutesAndFormat(new Date(), 5),
  },
];
export const a = 1;
