import {StyleSheet} from 'react-native';

const styleJoiner = (...arg) => StyleSheet.flatten(arg);
export default styleJoiner;
