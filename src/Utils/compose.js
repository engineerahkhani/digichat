// const compose = (...functions) => (args) =>
//   functions.reduceRight((arg, fn) => fn(arg), args);
const compose = (...fns) =>
  fns.reduceRight((f, g) => (...args) => g(f(...args)));

export default compose;
