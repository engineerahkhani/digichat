let key = 0;

const getNewKey = () => {
  key += 1;
  return key;
};

export default getNewKey;
