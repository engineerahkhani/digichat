import {Platform, StyleSheet} from 'react-native';
import colors from './Colors';

const theme = StyleSheet.create({
  commonTextStyle: {
    color: colors.lightBlack,
    fontSize: 14,
    fontStyle: 'normal',
    fontWeight: 'normal',
  },
  flexCenter: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  fontBold: {
    fontWeight: 'bold',
  },
  shadow: {
    elevation: 2,
    shadowColor: 'rgba(129, 133, 139, 0.2)',
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowOpacity: 1,
  },
});
export default theme;
