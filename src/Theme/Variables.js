import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');
export default {
  gutter: 16,
  width,
  height,
};
