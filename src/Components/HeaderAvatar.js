import React from 'react';
import {TouchableOpacity, StyleSheet, Image, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {colors, variables, theme} from '../Theme';
import {consumer} from '../Const/Const';
import Text from './Common/Text';

const HeaderMenuButton = () => {
  const navigation = useNavigation();
  const navigateToProfile = () =>
    navigation.navigate('ProfileScreen', {user: consumer});

  return (
    <TouchableOpacity onPress={navigateToProfile} style={styles.wrapper}>
      <Image
        source={consumer.image}
        style={styles.avatar}
        resizeMode="contain"
      />
      <View>
        <Text style={styles.name}>{consumer.name}</Text>
        <Text style={styles.time}>last seen recently</Text>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  avatar: {
    alignSelf: 'flex-end',
    borderRadius: 100,
    height: 35,
    marginHorizontal: variables.gutter,
    paddingVertical: variables.gutter / 2,
    width: 35,
  },
  name: {
    color: colors.white,
    fontSize: 13,
    ...theme.fontBold,
  },
  time: {
    color: colors.gray.light1,
    fontSize: 12,
  },
  wrapper: {
    flexDirection: 'row',
  },
});
export default HeaderMenuButton;
