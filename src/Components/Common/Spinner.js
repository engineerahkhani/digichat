import React from 'react';
import {ActivityIndicator} from 'react-native';
import {colors} from '../../Theme';

const Spinner = ({color, size, animating, hidesWhenStopped}) => (
  <ActivityIndicator
    color={color}
    size={size}
    animating={animating}
    hidesWhenStopped={hidesWhenStopped}
  />
);

Spinner.defaultProps = {
  animating: true,
  color: colors.primary,
  hidesWhenStopped: true,
  size: 'large',
};

export default Spinner;
