import React from 'react';
import {
  Modal as ModalRN,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import {styleJoiner} from '../../Utils';

const preventParentEvent = (e) => e.stopPropagation();

const Modal = ({
  children,
  style,
  transparent,
  animationType,
  visible,
  onRequestClose,
}) => (
  <ModalRN
    transparent={transparent}
    visible={visible}
    onRequestClose={onRequestClose}
    animationType={animationType}
    onBackdropPress={onRequestClose}>
    <TouchableWithoutFeedback onPress={onRequestClose}>
      <View style={styleJoiner(styles.container, style)}>
        <TouchableWithoutFeedback onPress={preventParentEvent}>
          {children}
        </TouchableWithoutFeedback>
      </View>
    </TouchableWithoutFeedback>
  </ModalRN>
);

Modal.defaultProps = {
  animationType: 'none',
  hasCloseButton: undefined,
  hideCloseBtn: undefined,
  onRequestClose: () => null,
  style: undefined,
  transparent: true,
  visible: true,
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'center',
    position: 'relative',
  },
});

export default Modal;
