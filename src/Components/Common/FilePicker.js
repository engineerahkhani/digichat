import React, {useState} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Button from './Button';
import Modal from './Modal';
import {styleJoiner} from '../../Utils';
import {theme, variables, colors} from '../../Theme';

const FilePicker = ({label, value, handleConfirm}) => {
  const [visible, setVisible] = useState(false);
  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const openCamera = () => {
    ImagePicker.openCamera({
      cropping: true,
      includeBase64: true,
    }).then((image) => {
      handleConfirm(image);
    });
    hideModal();
  };
  const openGallery = () => {
    ImagePicker.openPicker({
      cropping: true,
      includeBase64: true,
    }).then((image) => {
      handleConfirm(image);
    });
    hideModal();
  };

  return (
    <>
      <Button onPress={showModal} label={label} />
      {!!value && <Image style={styles.image} source={{uri: value}} />}
      <Modal onRequestClose={hideModal} visible={visible}>
        <View style={styleJoiner(styles.modalWrapper, theme.paper)}>
          <Button
            style={styles.modalActionButton}
            label="Gallery"
            onPress={openGallery}
            bold
            iconSiz={30}
            iconName="photo-library"
            labelStyle={styles.labelStyle}
          />
          <Button
            onPress={openCamera}
            style={styles.modalActionButton}
            label="Camera"
            bold
            iconName="photo-camera"
            iconSiz={30}
            labelStyle={styles.labelStyle}
          />
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#eeeeee',
    flex: 1,
    height: 50,
    justifyContent: 'center',
    margin: 10,
  },
  buttonsWrapper: {
    backgroundColor: '#fafafa',
    borderTopColor: '#bdbdbd',
    borderTopWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  image: {
    alignSelf: 'center',
    backgroundColor: '#fff',
    height: 100,
    marginTop: 8,
    resizeMode: 'center',
    width: 220,
  },
  labelStyle: {
    fontSize: 15,
    marginLeft: variables.gutter,
  },
  modalActionButton: {
    flexGrow: 1,
    paddingVertical: 24,
  },
  modalTitle: {
    fontSize: 20,
    marginBottom: variables.gutter,
    marginLeft: 24,
    ...theme.fontBold,
  },
  modalWrapper: {
    // paddingTop: 20,
    width: variables.width - 2 * variables.gutter,
    backgroundColor: '#fff',
  },
  signature: {
    borderTopColor: colors.border,
    borderTopWidth: StyleSheet.hairlineWidth * 2,
    height: variables.height * 0.6,
    // width: '100%',
  },
  submitBtn: {backgroundColor: colors.primary, flexGrow: 2},
});
export default FilePicker;
