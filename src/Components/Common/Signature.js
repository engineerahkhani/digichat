import SignatureCapture from 'react-native-signature-capture';
import React, {useState, useRef} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import Button from './Button';
import Modal from './Modal';
import Text from './Text';
import {styleJoiner} from '../../Utils';
import {theme, variables, colors} from '../../Theme';

const Signature = ({label, value, modalTitle, handleConfirm}) => {
  const [visible, setVisible] = useState(false);
  const signatureRef = useRef(null);
  const showModal = () => setVisible(true);
  const saveSign = () => {
    signatureRef.current.saveImage();
  };
  const resetSign = () => {
    setVisible(false);
  };
  const onSaveEvent = (result) => {
    setVisible(false);
    handleConfirm(result);
  };

  return (
    <>
      <Button onPress={showModal} label={label} />
      {!!value && <Image style={styles.image} source={{uri: value}} />}
      <Modal visible={visible}>
        <View style={styleJoiner(styles.modalWrapper, theme.paper)}>
          <Text style={styles.modalTitle}>{modalTitle}</Text>
          <SignatureCapture
            style={styles.signature}
            ref={signatureRef}
            onSaveEvent={onSaveEvent}
            saveImageFileInExtStorage={false}
            showNativeButtons={false}
            showTitleLabel={false}
            showBorder={false}
            viewMode="portrait"
          />
          <View style={styles.buttonsWrapper}>
            <Button
              onPress={saveSign}
              primary
              style={styleJoiner(styles.modalActionButton, styles.submitBtn)}
              label="OK"
              bold
            />
            <Button
              style={styles.modalActionButton}
              label="Cancel"
              onPress={resetSign}
              bold
            />
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#eeeeee',
    flex: 1,
    height: 50,
    justifyContent: 'center',
    margin: 10,
  },
  buttonsWrapper: {
    backgroundColor: '#fafafa',
    borderTopColor: '#bdbdbd',
    borderTopWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    // width,
    // marginTop: variables.gutter,
  },
  image: {
    alignSelf: 'center',
    backgroundColor: '#fff',
    height: 100,
    marginTop: 8,
    resizeMode: 'center',
    width: 220,
  },
  modalActionButton: {
    flexGrow: 1,
    paddingHorizontal: 25,
    paddingVertical: variables.gutter,
  },
  modalTitle: {
    fontSize: 20,
    marginBottom: variables.gutter,
    marginLeft: 24,
    ...theme.fontBold,
  },
  modalWrapper: {
    backgroundColor: '#fff',
    paddingTop: 20,
    width: variables.width - 2 * variables.gutter,
  },
  signature: {
    borderTopColor: colors.border,
    borderTopWidth: StyleSheet.hairlineWidth * 2,
    height: variables.width * 0.9,
    // width: '100%',
  },
  submitBtn: {backgroundColor: colors.primary, flexGrow: 2},
});
export default Signature;
