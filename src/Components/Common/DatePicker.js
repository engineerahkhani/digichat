import React, {useState} from 'react';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Ripple from './Ripple';
import MaterialTextField from './MaterialInput';

const DatePicker = ({label, value, handleConfirm, disabled}) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const formattedDate = `${value.getFullYear()}-${
    value.getMonth() + 1
  }-${value.getDate()} ${value.getHours()}:${value.getMinutes()}`;
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const onConfirm = (date) => {
    hideDatePicker();
    console.warn('A date has been picked: ', date);
    handleConfirm(date);
  };

  return (
    <>
      <Ripple disabled={disabled} onPress={showDatePicker}>
        <MaterialTextField
          disabled={disabled}
          label={label}
          value={formattedDate}
        />
      </Ripple>
      <DateTimePickerModal
        date={value}
        isVisible={isDatePickerVisible}
        mode="datetime"
        onConfirm={onConfirm}
        onCancel={hideDatePicker}
      />
    </>
  );
};

export default DatePicker;
