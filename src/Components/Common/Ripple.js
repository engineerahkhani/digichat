import React from 'react';
import CpRipple from 'react-native-material-ripple';

const Ripple = (props) => {
  return (
    <CpRipple
      rippleOpacity={0.15}
      rippleDuration={250}
      //  rippleContainerBorderRadius={0}
      {...props}>
      {props.children}
    </CpRipple>
  );
};
export default Ripple;
