import React from 'react';
import {Text as RNText} from 'react-native';
import {styleJoiner} from '../../Utils';
import {theme} from '../../Theme';

const Text = ({children, fa, style}) => {
  return (
    <RNText
      style={styleJoiner(theme.commonTextStyle, theme.fontPrimary, style)}>
      {children}
    </RNText>
  );
};

export default Text;
