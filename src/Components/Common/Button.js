import React, {useCallback} from 'react';
import {StyleSheet} from 'react-native';
import {colors, theme, variables} from '../../Theme';
import {styleJoiner} from '../../Utils';
import {isString} from 'lodash';
import Icon from './Icon';
import Text from './Text';
import Spinner from './Spinner';
import Ripple from './Ripple';

const Button = ({
  loading,
  loadingColor,
  loadingSize,
  iconName,
  iconSize,
  iconColor,
  label,
  labelStyle,
  primary,
  secondary,
  transparent,
  style,
  underlayColor,
  disabled,
  reverse,
  passive,
  bold,
  small,
  onPress,
  iconStyle,
  iconType,
  ...rest
}) => {
  const onPressHandler = useCallback(() => {
    global.requestAnimationFrame(onPress);
  }, [onPress]);

  return (
    <Ripple
      disabled={disabled}
      underlayColor={underlayColor}
      style={styleJoiner(
        styles.default,
        small && styles.smallWrapper,
        primary && styles.primary,
        secondary && styles.secondary,
        transparent && styles.transparent,
        style,
        disabled && styles.disabled,
        reverse && styles.reverse,
      )}
      onPress={onPressHandler}
      {...rest}>
      <>
        {!!iconName && !loading && (
          <Icon
            name={iconName}
            color={iconColor}
            style={iconStyle}
            size={iconSize}
            type={iconType}
          />
        )}
        {label && isString(label) && (
          <Text
            style={styleJoiner(
              styles.defaultText,
              labelStyle,
              bold && theme.fontBold,
              primary && styles.primaryText,
              secondary && styles.secondaryText,
              label && !!iconName && styles.textAlongSideIcon,
              reverse && label && !!iconName && styles.textAlongSideIconReverse,
              small && styles.small,
              passive && styles.passive,
            )}>
            {label}
          </Text>
        )}
        {label && !isString(label) && label}
        {loading && <Spinner size="small" color={colors.white} />}
      </>
    </Ripple>
  );
};

Button.defaultProps = {
  bold: undefined,
  disabled: undefined,
  iconColor: colors.lightBlack,
  iconName: '',
  iconSize: 24,
  iconStyle: undefined,
  iconType: undefined,
  label: undefined,
  labelStyle: undefined,
  loading: false,
  loadingColor: colors.lightBlack,
  loadingSize: 24,
  onPress: () => {},
  passive: undefined,
  primary: undefined,
  reverse: undefined,
  secondary: undefined,
  small: undefined,
  style: undefined,
  underlayColor: colors.linkUnderlayColor,
};
Button.whyDidYouRender = true;

const styles = StyleSheet.create({
  default: {
    alignItems: 'center',
    backgroundColor: colors.passiveLight,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: variables.gutter / 2,
  },
  defaultText: {
    color: colors.lightBlack,
    fontSize: 15,
  },
  disabled: {
    backgroundColor: colors.passive,
  },
  passive: {
    color: colors.gray.g3,
  },
  primary: {
    backgroundColor: colors.primary,
  },
  primaryText: {
    color: colors.white,
  },
  reverse: {flexDirection: 'row'},
  secondary: {
    backgroundColor: colors.transparent.invisible,
    borderColor: colors.primary,
    borderWidth: 1,
  },
  secondaryText: {
    color: colors.primary,
  },
  small: {
    fontSize: 12,
    marginRight: 8,
  },
  smallWrapper: {paddingVertical: variables.gutter / 4},
  textAlongSideIcon: {
    marginRight: 8,
  },
  textAlongSideIconReverse: {
    marginLeft: 13,
  },
  transparent: {
    backgroundColor: colors.transparent.invisible,
  },
});
export default Button;
