import React from 'react';
import {StyleSheet} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {styleJoiner} from '../../Utils';

const Icon = ({size, color, name, style}) => (
  <MaterialIcons
    name={name}
    size={size}
    color={color}
    style={styleJoiner(styles.transparent, style)}
  />
);

Icon.defaultProps = {
  size: 24,
};
const styles = StyleSheet.create({
  transparent: {backgroundColor: 'transparent'},
});
export default Icon;
