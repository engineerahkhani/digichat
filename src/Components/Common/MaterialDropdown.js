import React from 'react';
import {Dropdown} from 'react-native-material-dropdown';
import {colors} from '../../Theme';
import MaterialInput from './MaterialInput';
import {styleJoiner} from '../../Utils';
import Icon from './Icon';

const MaterialDropdown = ({
  disabled,
  valueExtractor,
  data,
  onChangeText,
  placeholder,
  label,
  value,
}) => {
  const filled = value && !disabled;

  return (
    <Dropdown
      disabled={disabled}
      valueExtractor={valueExtractor}
      onChangeText={onChangeText}
      value={value}
      data={data}
      style={styleJoiner(filled && {color: colors.primary})}
      renderBase={() => (
        <MaterialInput
          placeholder={placeholder}
          label={label}
          editable={false}
          value={value}
          renderRightAccessory={() => (
            <Icon
              size={22}
              color={disabled ? 'rgba(0, 0, 0, 0.38)' : '#737373'}
              name="arrow-drop-down"
            />
          )}
        />
      )}
    />
  );
};
export default MaterialDropdown;
