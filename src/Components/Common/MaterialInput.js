import React, {useEffect, useState} from 'react';
import {TextInput} from 'react-native';
import {colors} from '../../Theme';

const MaterialInput = ({value, onChangeText, ...props}) => {
  const [inputValue, setInputValue] = useState(value);
  useEffect(() => {
    setInputValue(value);
  }, [value]);
  const onChangeTextHandler = (text) => {
    setInputValue(text);
    onChangeText(text);
  };
  return (
    <TextInput
      {...props}
      onChangeText={onChangeTextHandler}
      value={inputValue}
      placeholderTextColor={colors.white}
      underlineColorAndroid={colors.lightBlack}
    />
  );
};
export default MaterialInput;
