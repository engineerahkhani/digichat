import React from 'react';
import {View, Image, StyleSheet} from 'react-native';
import Text from './Common/Text';
import {colors, variables} from '../Theme';
import {styleJoiner} from '../Utils';

const MessageRow = ({message, avatar, isAuthUser, date}) => {
  return (
    <View style={styleJoiner(styles.root, isAuthUser && styles.convertedRoot)}>
      <Image
        source={avatar}
        style={styleJoiner(styles.avatar, isAuthUser && styles.convertedAvatar)}
        resizeMode="contain"
      />
      <View
        style={styleJoiner(
          styles.message,
          isAuthUser && styles.convertedMessage
        )}>
        <View styles={styles.textWrapper}>
          <Text style={styles.text}>{message}</Text>
        </View>
        <Text
          style={styleJoiner(styles.date, isAuthUser && styles.dateReverted)}>
          {date}
        </Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  avatar: {
    alignSelf: 'flex-end',
    borderRadius: 100,
    height: 35,
    marginRight: variables.gutter / 2,
    width: 35,
  },
  convertedAvatar: {marginLeft: variables.gutter / 2, marginRight: 0},
  convertedMessage: {
    backgroundColor: colors.info,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 0,
  },
  convertedRoot: {
    flexDirection: 'row-reverse',
    paddingHorizontal: variables.gutter,
    paddingVertical: variables.gutter / 2,
  },
  date: {
    alignSelf: 'flex-end',
    color: '#fff',
    fontSize: 11,
    marginTop: variables.gutter / 2,
  },
  dateReverted: {alignSelf: 'flex-start'},
  message: {
    backgroundColor: colors.primary,
    borderBottomLeftRadius: 0,
    borderRadius: 15,
    paddingHorizontal: variables.gutter,
    paddingVertical: variables.gutter / 2,
  },
  root: {
    flexDirection: 'row',
    paddingHorizontal: variables.gutter,
  },
  text: {
    color: colors.white,
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 14,
    lineHeight: 20,
  },
  textWrapper: {flexDirection: 'row'},
});
export default MessageRow;
