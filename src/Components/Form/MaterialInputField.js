import React from 'react';
import {Field} from 'formik';
import MaterialInput from '../Common/MaterialInput';
import FieldWrapper from './FieldWrapper';

const MaterialInputWithFormik = ({
  name,
  form: {errors, touched, handleBlur, handleChange},
  field: {value},
  ...props
}) => {
  return (
    <FieldWrapper errors={errors} touched={touched} name={name}>
      <MaterialInput
        {...props}
        onChangeText={handleChange(name)}
        onBlur={handleBlur(name)}
        value={value}
      />
    </FieldWrapper>
  );
};
export default (props) => (
  <Field name={props.name}>
    {({field, form}) => (
      <MaterialInputWithFormik {...props} form={form} field={field} />
    )}
  </Field>
);
