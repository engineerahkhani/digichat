import React, {memo} from 'react';
import {StyleSheet} from 'react-native';
import Text from '../Common/Text';
import {colors} from '../../Theme';

const FieldWrapper = ({children, name, errors, touched}) => {
  return (
    <>
      {children}
      {errors[name] && touched[name] ? (
        <Text style={styles.error}>{errors[name]}</Text>
      ) : null}
    </>
  );
};

FieldWrapper.whyDidYouRender = true;

const styles = StyleSheet.create({
  error: {
    color: colors.error,
    fontSize: 12,
  },
});
FieldWrapper.defaultProps = {};
const MemoFieldWrapper = memo(FieldWrapper);
export default MemoFieldWrapper;
