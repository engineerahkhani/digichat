import React from 'react';
import {Field} from 'formik';
import Button from '../Common/Button';

const SubmitButton = (props) => (
  <Field>
    {({form}) => (
      <Button
        {...props}
        onPress={form.handleSubmit}
        disabled={form.isSubmitting}
        loading={form.isSubmitting}
      />
    )}
  </Field>
);
export default SubmitButton;
