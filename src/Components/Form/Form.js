import React from 'react';
import {Keyboard} from 'react-native';
import {Formik} from 'formik';

const Form = (props) => {
  const {initialValues, children, onSubmit} = props;
  const onSubmitHandler = async (values, {resetForm}) => {
    Keyboard.dismiss();
    onSubmit(values, resetForm);
  };

  return (
    <Formik initialValues={initialValues} onSubmit={onSubmitHandler}>
      {children}
    </Formik>
  );
};

export default Form;
