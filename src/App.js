import React from 'react';
import {StyleSheet, StatusBar, SafeAreaView} from 'react-native';
import 'react-native-gesture-handler';
import RootNavigation from './Navigation/RootNavigation';
import {colors} from './Theme';

const App = () => {
  return (
    <SafeAreaView style={styles.wrap}>
      <StatusBar backgroundColor={colors.black} barStyle="dark-content" />
      <RootNavigation />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({wrap: {flex: 1}});

export default App;
