import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {colors, variables, theme} from '../Theme';
import Text from '../Components/Common/Text';

const ProfileScreen = ({route}) => {
  const {user} = route.params;

  return (
    <View style={styles.root}>
      <View>
        <Image source={user.image} style={styles.avatar} resizeMode="contain" />
        <Text style={styles.name}>{user.name}</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  avatar: {
    alignSelf: 'flex-end',
    borderRadius: 100,
    height: 75,
    marginHorizontal: variables.gutter,
    paddingVertical: variables.gutter / 2,
    width: 75,
  },
  name: {
    color: colors.white,
    fontSize: 17,
    ...theme.fontBold,
    marginTop: variables.gutter,
    textAlign: 'center',
  },
  root: {
    alignItems: 'center',
    backgroundColor: colors.black,
    flex: 1,
    justifyContent: 'center',
  },
});
export default ProfileScreen;
