import React, {useState} from 'react';
import {StyleSheet, FlatList, View} from 'react-native';
import Form from '../Components/Form/Form';
import {colors, variables} from '../Theme';
import MessageRow from '../Components/MessageRow';
import {initMessage, authUser, consumer} from '../Const/Const';
import MaterialInputField from '../Components/Form/MaterialInputField';
import {formatDate, getNewKey} from '../Utils';
import SubmitButton from '../Components/Form/SubmitButton';

const consumerMessage = (message) => ({
  id: getNewKey(),
  user: consumer,
  message: message.toUpperCase(),
  date: formatDate(new Date()),
});
const ChatScreen = () => {
  const [messages, setMessages] = useState(() => initMessage.reverse());
  const onsubmitHandler = ({message}, resetForm) => {
    const newMessage = {
      id: getNewKey(),
      user: authUser,
      message,
      date: formatDate(new Date()),
    };
    setMessages((prev) => [newMessage, ...prev]);
    resetForm();
    setTimeout(() => {
      setMessages((prev) => [consumerMessage(message), ...prev]);
    }, 2000);
  };
  const renderItem = ({item}) => (
    <MessageRow
      message={item.message}
      avatar={item.user.image}
      isAuthUser={item.user.id === authUser.id}
      date={item.date}
    />
  );
  const keyExtractor = (item) => `${item.id}`;

  return (
    <View style={styles.root}>
      <FlatList
        contentContainerStyle={styles.flatList}
        data={messages}
        keyExtractor={keyExtractor}
        inverted
        renderItem={renderItem}
      />
      <Form initialValues={{message: ''}} onSubmit={onsubmitHandler}>
        <View style={styles.formWrapper}>
          <MaterialInputField
            style={styles.input}
            name="message"
            placeholder="write message..."
            multiline
            numberOfLines={6}
          />
          <SubmitButton
            style={styles.sendBtn}
            iconName="send"
            iconSize={25}
            iconColor={colors.primary}
          />
        </View>
      </Form>
    </View>
  );
};

const styles = StyleSheet.create({
  flatList: {paddingVertical: variables.gutter},
  formWrapper: {
    alignItems: 'center',
    backgroundColor: colors.lightBlack,
    borderTopColor: colors.gray.g0,
    borderTopWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    paddingHorizontal: variables.gutter,
    paddingRight: 0,
    paddingVertical: variables.gutter / 2,
    // maxHeight:40,
  },
  input: {
    backgroundColor: colors.lightBlack,
    color: colors.white,
    flexBasis: 0,
    flexGrow: 8,
    fontSize: 15,
    height: 40,
    lineHeight: 40,
    paddingBottom: 0,
    paddingRight: variables.gutter,
    paddingTop: 0,
    textAlignVertical: 'center',
  },
  root: {
    backgroundColor: colors.black,
    flex: 1,
  },
  sendBtn: {
    backgroundColor: colors.lightBlack,
    flexBasis: 0,
    flexGrow: 1,
    padding: variables.gutter / 2,
  },
});

export default ChatScreen;
